#!/bin/bash


# groupadd -g $PGID www-data
# useradd -u $PUID -g www-data www-data

CONFIG=/config
WWW=/www
ORIGIN=/home/nginx-php/origin

NGINX_ORIGIN=$ORIGIN/nginx.site
NGINX_SYSTEM=/etc/nginx/sites-enabled/default
NGINX_CONFIG=$CONFIG/nginx.site

PHP_INI_ORIGIN=$ORIGIN/php.ini # Copied in Dockerfile
PHP_INI_SYSTEM=/etc/php/${PHP_VERSION}/fpm/php.ini
PHP_INI_CONFIG=$CONFIG/php.ini

SCRIPT_PRE_START_ORIGIN=$ORIGIN/pre-start.sh
SCRIPT_PRE_START_CONFIG=$CONFIG/pre-start.sh

SCRIPT_POST_START_ORIGIN=$ORIGIN/post-start.sh
SCRIPT_POST_START_CONFIG=$CONFIG/post-start.sh

CRON_CONFIG_ORIGIN=$ORIGIN/cron.conf
CRON_CONFIG_CONFIG=$CONFIG/cron.conf
CRON_LOGS=/var/log/cron.log

function cp_if_not_exists {
  src=$1
  dst=$2
  if [[ ! -f "$dst" ]]; then
    echo "Create file : $dst"
    cp -f "$src" "$dst"
  fi
}

function service_start {
  service=$1
  status=$2
  if [[ "$status" == "true" ]]; then
    echo Start $service
    service $service start 2>&1 > /dev/null
  fi
}

function create_user {
  user=$1
  group=$2
  uid=$3
  gid=$4
  userdel $user
  echo -e "$user:x:${uid}:${gid}:$user:/$user:/bin/false\n" >> /etc/passwd
  echo -e "$group:x:${gid}:$group\n" >> /etc/group
}

function run_script {
  origin=$1
  config=$2
  enabled=$3
  if [[ $enabled == "true" ]]; then
    echo "Run $config"
    cp_if_not_exists $origin $config
    chmod +x $config
    $config
  fi
}

function start_mta {
    service_start sendmail $SENDMAIL
}

function start_php {
  if [[ $PHP == "true" ]]; then
    ln -sf $PHP_INI_CONFIG $PHP_INI_SYSTEM
    cp_if_not_exists $PHP_INI_ORIGIN $PHP_INI_CONFIG
    service_start php${PHP_VERSION}-fpm true
  else
    touch /var/log/php-void-fpm.log
  fi
}

function start_cron {
  if [[ $CRON == "true" ]]; then
    cp_if_not_exists $CRON_CONFIG_ORIGIN $CRON_CONFIG_CONFIG
    crontab $CRON_CONFIG_CONFIG
    touch $CRON_LOGS
    service_start cron true
  else
    touch /var/log/cron.log
  fi
}

function start_nginx {
#  echo "Start nginx with PUID=$PUID, PGID=$PGID"
  ln -sf $NGINX_CONFIG $NGINX_SYSTEM
  cp_if_not_exists $NGINX_ORIGIN $NGINX_CONFIG
  sed -i s/PHP_VERSION/$PHP_VERSION/g $NGINX_CONFIG
  service_start nginx true
}

function show_logs {
  echo "--------------------------------------------------------------------------------"
  tail -q -F /var/log/nginx/access.log /var/log/nginx/error.log /var/log/php*-fpm.log /var/log/cron.log
}

mkdir -p $CONFIG
mkdir -p $WWW

#chmod +x $CONFIG/*.sh

if [[ $PRE_START != "true" ]]; then PRE_START="false"; fi
echo "PRE_START  => $PRE_START"
if [[ $POST_START != "true" ]]; then POST_START="false"; fi
echo "POST_START => $POST_START"
if [[ $PHP != "false" ]]; then PHP="true"; fi
echo "PHP        => $PHP"
if [[ $SENDMAIL != "true" ]]; then SENDMAIL="false"; fi
echo "SENDMAIL   => $SENDMAIL"
if [[ $CRON != "true" ]]; then CRON="false"; fi
echo "CRON       => $CRON"

create_user www-data www-data $PUID $PGID
chown -R www-data\: $CONFIG

run_script $SCRIPT_PRE_START_ORIGIN $SCRIPT_PRE_START_CONFIG "$PRE_START";
start_mta;
start_php;
start_cron;
start_nginx;
run_script $SCRIPT_POST_START_ORIGIN $SCRIPT_POST_START_CONFIG "$POST_START";

#ls -l /config/

show_logs;


