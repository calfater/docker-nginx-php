# Web server with php, MTA, cron

## What's inside
 * [Nginx](https://www.nginx.com)
 * [PHP](https://php.net) : [FastCGI Process Manager](https://www.php.net/manual/fr/install.fpm.php) 
 * MTA : [Sendmail](https://sendmail.org)
 * [Cron](http://man7.org/linux/man-pages/man5/crontab.5.html)
 
## Running

`docker run -d calfater/nginx-php`

`docker run -d -e ENV_VAR=value -e ANOTHER_ENV_VAR=another-value calfater/nginx-php`

### Env variables
 * PUID = **1000**
 * PGID = **1000**
 * PHP = **true** | false
 * SENDMAIL = true | **false** (if true : a `hostname` is required)
 * CRON = true | **false**
 * PRE_START = true | **false**
 * POST_START = true | **false**
